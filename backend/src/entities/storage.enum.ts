export enum StorageEnum {
  UPTOBOX = 'uptobox',
  GOOGLE_DRIVE = 'gdrive',
  NAS = 'nas',
  SSH = 'ssh',
  NONE = ''
}
