import got from 'got';
import {Database} from '../../database/database';
import {DebridersEnum} from '../../entities/debriders.enum';
import {AlldebridTorrentsDto} from '../../dtos/alldebrid-torrents.dto';
import {TorrentFromFrontDto} from '../../dtos/torrent-from-front.dto';
import {TorrentToFrontDTO} from '../../dtos/torrent-to-front.dto';
import {TorrentInDebriderInfos} from '../../entities/torrent-in-debrider-infos';
import {TorrentStatusEnum} from '../../entities/torrent-status.enum';
import {User} from '../../entities/user';
import {FileExtension, UptoboxFileCode} from '../../storage/uptobox/uptobox';
import {IDebrider} from '../i-debrider';

interface AddMagnetDto {
  status: string;
  data: {
    magnets: [
      {
        magnet: string,
        hash: string
        name: string,
        filename_original: string,
        size: number,
        ready: boolean,
        id: number
      }
    ]
  },
  error?: {
    message: string
  }
}

interface CreatePinDto {
  status: string;
  data: {
    pin: string;
    check: string;
    expires_in: number;
    user_url: string;
    base_url: string;
    check_url: string;
  }
}

interface CheckPinStatusDto {
  status: string;
  data: {
    apikey: string;
    activated: boolean;
    expires_in: number;
  }
}

interface GetMagnetStatusDto {
  status: string;
  data: {
    magnets: MagnetStatusDto;
  }
}

interface GetFilesAndListsDto {
  status: string;
  data: {
    magnets: MagnetFilesDto[]
  }
}

interface UnlockLinkDto {
  status: string;
  data: {
    link: string;
    host: string;
    filename: string;
    streaming: [];
    paws: boolean;
    filesize: number;
    id: string;
    path: []
  }
}

export interface UnlockedLink {
  link: string;
  fileName: string;
}

interface MagnetFilesDto {
  id: string;
  files: FilesDto[];
}

interface FilesDto {
  // Folder name of file name
  n: string;

  // file size
  s?: number;

  // The wanted link of the magnet we'll have to unlock later - only available for files
  l: string;

  // Sub nodes
  e?: FilesDto[];
}

interface MagnetStatusDto {
  id: number;
  filename: string;
  size: number;
  hash: string
  status: string;
  statusCode: number;
  downloaded: number;
  uploaded: number;
  seeders: number;
  downloadSpeed: number;
  processingPerc: number;
  uploadSpeed: number;
  uploadDate: number;
  completionDate: number;
  links: LinkMagnetDto[];
}

interface LinkMagnetDto {
  link: string;
  filename: string;
  size: number;
  files: [
    {
      n: FileName
    }
  ]
}

// TODO extract in value object
class UptoboxLink {
  magnetStatusDto: GetMagnetStatusDto;
  url: string;
  fileExtension: FileExtension;
  fileCode: UptoboxFileCode;

  constructor(magnetStatusDto: GetMagnetStatusDto) {
    this.magnetStatusDto = magnetStatusDto;

    // Many files wan be available to download, I am keeping file that is NOT nfo
    // (for now, should be improved to be able to work with multiple media files)
    const bestFile = magnetStatusDto.data.magnets.links.find(item => item.filename.match('.*(?!(.nfo))$')[0]);

    this.url = bestFile.link;
    this.fileExtension = new FileExtension(bestFile.filename.match('\.[0-9a-z]+$')[0]);
    this.fileCode = new UptoboxFileCode(magnetStatusDto.data.magnets.links[0].link.replace('https://uptobox.com/',''));
  }

  getFileCode(): UptoboxFileCode {
    const code = this.url.replace('https://uptobox.com/','');
    return new UptoboxFileCode(code);
  }
}

// TODO extract in value object
class FileName {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}



export class AllDebrid implements IDebrider {

  fileStructure: string[] = [];

  getLink(file: FilesDto): void {
    if (file.e) {
      file.e.map(subNode => {
        this.getLink(subNode);
      })
    } else {
      this.fileStructure.push(file.l)
    }
  }

  async getFilesLinksFromMagnetId(magnetId: number, user: User): Promise<string[]> {
    try {
      const response = await got(`https://api.alldebrid.com/v4/magnet/files?agent=lazyker&apikey=${user.settings.debriders.alldebrid.apiKey}&id[]=${magnetId}`, { json: true })

      const body = response.body as GetFilesAndListsDto;

      const files = body.data.magnets[0].files;

      files.map(file => {
        return this.getLink(file);
      })

      // return body.data.magnets[0].files.map(file => file.l).filter(file => file !== undefined);

      return this.fileStructure;
    } catch(error) {
      console.log(error.message);
    }
  }

  async unlockFilesLinks(filesLinks: string[], user: User): Promise<UnlockedLink[]> {
    try {

      const unlockedFilesLinksPromises = filesLinks.map(async link => {
        const response = await got(`https://api.alldebrid.com/v4/link/unlock?agent=lazyker&apikey=${user.settings.debriders.alldebrid.apiKey}&link=${link}`, { json: true })

        const body = response.body as UnlockLinkDto;

        return {
          link: body.data.link,
          fileName: body.data.filename
        }
      })

      const unlockedFiles = []

      for await (let value of unlockedFilesLinksPromises) {
        console.log(value);
        unlockedFiles.push(value);
      }

      return unlockedFiles;

    } catch(error) {
      console.log(error.message);
    }
  }

  async getUnlockedFilesLinks(magnetId: number, user: User): Promise<UnlockedLink[]> {
    const filesLinks = await this.getFilesLinksFromMagnetId(magnetId, user);
    return this.unlockFilesLinks(filesLinks, user);
  }

  async getUptoboxLink(magnetId: number, user: User): Promise<UptoboxLink> {
    try {

      // TODO: we should take the user api key

      const response = await got(`https://api.alldebrid.com/v4/magnet/status?agent=lazyker&apikey=${user.settings.debriders.alldebrid.apiKey}&id=${magnetId}`, { json: true })

      const body = response.body as GetMagnetStatusDto;

      return new UptoboxLink(body);
    } catch(error) {
      console.log(error.message);
    }
  }

  async addMagnetLink(magnetLink: string, user: User): Promise<TorrentInDebriderInfos> {
    try {

      // TODO: we should take the user api key

      const response = await got(`https://api.alldebrid.com/v4/magnet/upload?agent=lazyker&apikey=${user.settings.debriders.alldebrid.apiKey}&magnets[]=${magnetLink}`, { json: true })

      const body = response.body as AddMagnetDto;

      if (body.status === 'error') {
        throw new Error(body.error.message);
      }

      return new TorrentInDebriderInfos(body.data.magnets[0].id, body.data.magnets[0].ready);

    } catch(error) {
      console.log(error.message);
      throw new Error(`Unable to add magnet link to all debrid debrider -> ${error.message}`)
    }
  }

  static async listTorrents(user: User): Promise<TorrentToFrontDTO[]> {

    try {
      const apikey = await Database.getAlldebridApiKey(user);

      const response = await got(`https://api.alldebrid.com/v4/magnet/status?agent=lazyker&apikey=${apikey}`, { json: true });

      const alldebridTorrents = response.body as AlldebridTorrentsDto;

      return alldebridTorrents.data.magnets.map(alldebridTorrent => {

        let status: TorrentStatusEnum;

        switch(alldebridTorrent.statusCode) {
          case 0: {
            status = TorrentStatusEnum.QUEUED;
            break;
          }
          case 1: {
            status = TorrentStatusEnum.DOWNLOADING;
            break;
          }
          case 2: {
            status = TorrentStatusEnum.DOWNLOADING;
            break;
          }
          case 3: {
            status = TorrentStatusEnum.DOWNLOADING;
            break;
          }
          case 4: {
            status = TorrentStatusEnum.DOWNLOADED;
            break;
          }
          case 5: {
            status = TorrentStatusEnum.ERROR;
            break;
          }
          case 6: {
            status = TorrentStatusEnum.ERROR;
            break;
          }
          case 7: {
            status = TorrentStatusEnum.ERROR;
            break;
          }
          case 8: {
            status = TorrentStatusEnum.ERROR;
            break;
          }
          case 9: {
            status = TorrentStatusEnum.ERROR;
            break;
          }
          case 10: {
            status = TorrentStatusEnum.ERROR;
            break;
          }
          case 11: {
            status = TorrentStatusEnum.ERROR;
            break;
          }
        }

        return {
          filename: alldebridTorrent.filename,
          status: status,
          id: alldebridTorrent.id.toString(),
          progress: (alldebridTorrent.size/alldebridTorrent.downloaded)*100
        } as TorrentToFrontDTO
      });

    } catch(error) {
      console.log(error.message);
    }
  }

  static async deleteTorrent(user: User, torrentId: string): Promise<void> {
    try {
      const apikey = await Database.getAlldebridApiKey(user);
      await got(`https://api.alldebrid.com/v4/magnet/delete?agent=lazyker&apikey=${apikey}&id=${torrentId}`, { json: true });
    } catch(error) {
      throw new Error('Error in alldebrid-debrider -> ' + error.message);
    }
  }

  static async disconnect(user: User): Promise<void> {
    await Database.removeDebrider(user, DebridersEnum.ALLDEBRID);
  }

  static async getPinCode(): Promise<CreatePinDto> {
    try {

      // Get a fresh new pin
      const response = await got(`https://api.alldebrid.com/v4/pin/get?agent=lazyker`, { json: true })

      return response.body as CreatePinDto;

    } catch(error) {
      console.error(error.message);
    }
  }

  static async checkPinCodeStatus(user: User, pin: string, check: string): Promise<CheckPinStatusDto> {
    try {

      // Check status of a particular pin code
      const response = await got(`https://api.alldebrid.com/v4/pin/check?agent=lazyker&check=${check}&pin=${pin}`, { json: true })

      const pinStatus = response.body as CheckPinStatusDto;

      if (pinStatus.data.activated) {
        await this.storeAlldebridToken(user, pinStatus.data.apikey);
      }

      return pinStatus;

    } catch(error) {
      console.error(error.message);
    }
  }

  static async storeAlldebridToken(user: User, apiKey: string): Promise<string> {
    try {

      await Database.storeAlldebridApiKey(user, DebridersEnum.ALLDEBRID, apiKey);

      return apiKey;

    } catch(error) {
      console.error(error.message);
    }
  }

}
