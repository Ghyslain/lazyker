import * as admin from 'firebase-admin';
import {CurrentDownloadStatus} from '../entities/current-download-status';
import {CurrentDownloadStatusEnum} from '../entities/current-download-status-enum';
import {DebridersEnum} from '../entities/debriders.enum';
import {DownloadedTorrent} from '../entities/downloaded-torrent';
import {MediaInfos} from '../entities/media-infos';
import {StorageEnum} from '../entities/storage.enum';
import {TorrentInDebriderInfos} from '../entities/torrent-in-debrider-infos';
import {User, UserSettings} from '../entities/user';

export class Database {

  static db = admin.database();
  static usersRef = Database.db.ref("/users");

  static async store(user: User, path: string, objectToStore: any):Promise<any> {
    return await Database.usersRef
      .child(user.uid)
      .child(path)
      .set(objectToStore);
  }

  static async delete(user: User, path: string): Promise<void> {
    return await Database.usersRef
        .child(user.uid)
        .child(path)
        .remove();
  }

  static async storeAlldebridApiKey(user: User, debrider: DebridersEnum, apiKey: any): Promise<any> {
    return await Database.usersRef
      .child(user.uid)
      .child('settings')
      .child('debriders')
      .child(debrider)
      .set({apiKey: apiKey});
  }

  static async removeDebrider(user: User, debrider: DebridersEnum) {
    return await Database.usersRef
      .child(user.uid)
      .child('settings')
      .child('debriders')
      .child(debrider)
      .remove();
  }

  static async getSelectedStorage(user: User): Promise<StorageEnum> {
    const storageSnapshot = await Database.usersRef
      .child(user.uid)
      .child('settings')
      .child('storage')
      .child('selected')
      .once('value');

    return storageSnapshot.val() as StorageEnum
  }

  static async getSelectedDebrider(user: User): Promise<DebridersEnum> {
    const storageSnapshot = await Database.usersRef
        .child(user.uid)
        .child('settings')
        .child('debriders')
        .child('selected')
        .once('value');

    return storageSnapshot.val() as DebridersEnum
  }

  static async getUserSettings(user: User): Promise<UserSettings> {
    const snapshot = await Database.usersRef.child(user.uid).child('settings').once('value');
    return snapshot.val();
  }

  static async getAlldebridApiKey(user: User): Promise<string> {
    const storageSnapshot = await Database.usersRef
        .child(user.uid)
        .child('settings')
        .child('debriders')
        .child('alldebrid')
        .child('apiKey')
        .once('value');

    return storageSnapshot.val();
  }

  // DO NOT DELETE - will be used (probably)
  static async storeDownloadedTorrentInDebrider(user: User, torrent: TorrentInDebriderInfos, mediaInfos: MediaInfos):Promise<void> {
    const downloadedTorrent = new DownloadedTorrent(mediaInfos, torrent, new CurrentDownloadStatus(CurrentDownloadStatusEnum.FINISHED, 10, 10));
    return await Database.store(user, `/torrentsDownloadedInDebrider/${user.settings.debriders.selected}/${downloadedTorrent.id}`,downloadedTorrent);
  }

  static async storeDownloadedTorrentInStorage(user: User, torrent: TorrentInDebriderInfos, mediaInfos: MediaInfos):Promise<void> {
    const downloadedTorrent = new DownloadedTorrent(mediaInfos, torrent, new CurrentDownloadStatus(CurrentDownloadStatusEnum.FINISHED, 10, 10));
    return await Database.store(user, `/torrentsDownloadedInStorage/${user.settings.storages.selected}/${downloadedTorrent.id}`,downloadedTorrent);
  }

  static async deleteStorageDownload(user: User, downloadId: number): Promise<void> {
    return await Database.delete(user,`/torrentsDownloadedInStorage/${user.settings.storages.selected}/${downloadId}`);
  }

}
